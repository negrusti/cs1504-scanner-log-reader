﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using Microsoft.Win32;

namespace ScannerLogs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public class TextBoxStreamWriter : TextWriter
    {
        readonly System.Windows.Controls.TextBox _output;

        public TextBoxStreamWriter(System.Windows.Controls.TextBox output)
        {
            _output = output;
        }

        public override void Write(char value)
        {
            base.Write(value);
            _output.AppendText(value.ToString()); // When character data is written, append it to the text box.
        }

        public override Encoding Encoding => Encoding.UTF8;
    }

    public partial class MainWindow
    {
        Dictionary<int, string> codeTypes = new Dictionary<int, string>()
        {
            {0x16,  "Bookland"},
            {0x02,  "Codabar"},
            {0x0c,  "Code 11"},
            {0x20,  "Code 32"},
            {0x03,  "Code 128"},
            {0x01,  "Code 39"},
            {0x13,  "Code 39 Full ASCII"},
            {0x07,  "Code 93"},
            {0x1d,  "Composite"},
            {0x17,  "Coupon"},
            {0x04,  "D25"},
            {0x1b,  "Data Matrix"},
            {0x0f,  "EAN-128"},
            {0x0b,  "EAN-13"},
            {0x4b,  "EAN-13+2"},
            {0x8b,  "EAN-13+5"},
            {0x0a,  "EAN-8"},
            {0x4a,  "EAN-8+2"},
            {0x8a,  "EAN-8+5"},
            {0x05,  "IATA"},
            {0x19,  "ISBT-128"},
            {0x21,  "ISBT-128 Concatenated"},
            {0x06,  "ITF"},
            {0x28,  "Macro PDF"},
            {0x0E,  "MSI"},
            {0x11,  "PDF-417"},
            {0x26,  "Postbar (Canada)"},
            {0x1e,  "Postnet (US)"},
            {0x23,  "Postal (Australia)"},
            {0x22,  "Postal (Japan)"},
            {0x27,  "Postal (UK)"},
            {0x1c,  "QR Code"},
            {0x31,  "RSS Limited"},
            {0x30,  "RSS-14"},
            {0x32,  "RSS Expanded"},
            {0x24,  "Signature"},
            {0x15,  "Trioptic Code 39"},
            {0x08,  "UPCA"},
            {0x48,  "UPCA+2"},
            {0x88,  "UPCA+5"},
            {0x09,  "UPCE"},
            {0x49,  "UPCE+2"},
            {0x89,  "UPCE+5"},
            {0x10,  "UPCE1"},
            {0x50,  "UPCE1+2"},
            {0x90,  "UPCE1+5"}
        };

        private enum Csp2Status
        {
            STATUS_OK = 0,
            COMMUNICATIONS_ERROR = -1,
            BAD_PARAM = -2,
            SETUP_ERROR = -3,
            INVALID_COMMAND_NUMBER = -4,
            COMMAND_LRC_ERROR = -7,
            RECEIVED_CHARACTER_ERROR = -8,
            GENERAL_ERROR = -9,
            FILE_NOT_FOUND = 2,
            ACCESS_DENIED = 5
        }

        private enum Csp2Data
        {
            DATA_AVAILABLE = 0,
            NO_DATA_AVAILABLE = 1
        }

        private enum ComPort
        {
            COM1 = 0,
            COM2 = 1,
            COM3 = 2,
            COM4 = 3,
            COM5 = 4,
            COM6 = 5,
            COM7 = 6,
            COM8 = 7,
            COM9 = 8,
            COM10 = 9,
            COM11 = 10,
            COM12 = 11,
            COM13 = 12,
            COM14 = 13,
            COM15 = 14,
            COM16 = 15
        }

        public MainWindow()
        {
            InitializeComponent();
            TextWriter writer = new TextBoxStreamWriter(textConsole);
            Console.SetOut(writer);

            string[] ports = SerialPort.GetPortNames();
            Ports.ItemsSource = ports;

            try
            {
                File.Text = Properties.Settings.Default.output;
                Ports.SelectedItem = Array.IndexOf(ports, Properties.Settings.Default.port) > -1 ? Properties.Settings.Default.port : ports[0];
            }

            catch
            {

            }


        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            Properties.Settings.Default.output = File.Text;
            Properties.Settings.Default.port = Ports.Text;
            Properties.Settings.Default.Save();
        }


        private void SaveAs_OnClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog
            {
                FileName = "ScannerLog.txt",
                Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*"
            };

            if (savefile.ShowDialog() == true)
            {
                File.Text = savefile.FileName;
            }
        }

        uint swapEndianness(uint x)
        {
            return ((x & 0x000000ff) << 24) +  // First byte
                   ((x & 0x0000ff00) << 8) +   // Second byte
                   ((x & 0x00ff0000) >> 8) +   // Third byte
                   ((x & 0xff000000) >> 24);   // Fourth byte
        }

        private void ReadLog_OnClick(object sender, RoutedEventArgs e)
        {
            SaveAs.IsEnabled = false;
            ReadLog.IsEnabled = false;

            byte[] packet = new byte[256];

            try
            {
                var port = (ComPort)Enum.Parse(typeof(ComPort), Ports.Text);
                textConsole.Text = "";

                StreamWriter file = new StreamWriter(File.Text, true);

                Console.WriteLine("Init: {0}", csp2Init(port));
                Console.WriteLine("Data: {0}", csp2DataAvailable());

                var codesCount = csp2ReadData();
                var rtcMode = csp2GetRTCMode();
                Console.WriteLine("RTC Mode: {0}", rtcMode);

                if (codesCount < 0)
                {
                    Console.WriteLine("Reading data: {0}\n", (Csp2Status)codesCount);
                }
                else
                {
                    Console.WriteLine("Number of barcodes: {0}\n", codesCount);
                }

                for (var i = 0; i < (int)codesCount; i++)
                {
                    var packetLen = csp2GetPacket(packet, i, 256);
                    if (packetLen > 0)
                    {
                        var len = packet[0];
                        var type = packet[1];
                        var timestamp = BitConverter.ToUInt32(packet, len - 3);
                        timestamp = swapEndianness(timestamp);
                        var years = (timestamp & 0x0000003f) + 2000;
                        var months = timestamp >> 6 & 0x0000000f;
                        var days = timestamp >> 10 & 0x0000001f;
                        var hours = timestamp >> 15 & 0x0000001f;
                        var minutes = timestamp >> 20 & 0x0000003f;
                        var seconds = timestamp >> 26 & 0x0000003f;
                        var text = Encoding.UTF8.GetString(packet, 2, len - 5).Trim();
                        Console.WriteLine(@"{7} - {3,2:D2}:{4,2:D2}:{5,2:D2} {2,2:D2}.{1,2:D2}.{0,4:D4} - {6}", years, months, days, hours, minutes, seconds, text, codeTypes[type]);
                        file.WriteLine("{6},{3,2:D2}:{4,2:D2}:{5,2:D2},{2,2:D2}.{1,2:D2}.{0,4:D4}", years, months, days, hours, minutes, seconds, text);
                    }
                }
                csp2Restore();
                file.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            SaveAs.IsEnabled = true;
            ReadLog.IsEnabled = true;
        }

        [DllImport("Csp2.dll")]
        private static extern Csp2Status csp2Init(ComPort nComPort);
        [DllImport("Csp2.dll")]
        private static extern Csp2Data csp2DataAvailable();
        [DllImport("Csp2.dll")]
        private static extern int csp2ReadData();
        [DllImport("Csp2.dll")]
        private static extern Csp2Status csp2GetPacket(byte[] szBarData, int nBarcodeNumber, int nMaxLength);
        [DllImport("Csp2.dll")]
        private static extern int csp2GetRTCMode();
        [DllImport("Csp2.dll")]
        private static extern Csp2Status csp2Restore();
        [DllImport("Csp2.dll")]
        private static extern Csp2Status csp2SetTime(byte[] aTimeBuf);
        [DllImport("Csp2.dll")]
        private static extern Csp2Status csp2ClearData();

        private void ClearData_OnClick(object sender, RoutedEventArgs e)
        {
            var port = (ComPort)Enum.Parse(typeof(ComPort), Ports.Text);
            textConsole.Text = "";
            Console.WriteLine("Init: {0}", csp2Init(port));
            Console.WriteLine("Clear Data: {0}", csp2ClearData());
            csp2Restore();
        }

        private void SetTime_OnClick(object sender, RoutedEventArgs e)
        {
            var port = (ComPort)Enum.Parse(typeof(ComPort), Ports.Text);
            textConsole.Text = "";
            Console.WriteLine("Init: {0}", csp2Init(port));
            byte[] timeset = new byte[6];
            DateTime now = DateTime.Now;
            timeset[0] = (byte)now.Second;
            timeset[1] = (byte)now.Minute;
            timeset[2] = (byte)now.Hour;
            timeset[3] = (byte)now.Day;
            timeset[4] = (byte)now.Month;
            timeset[5] = (byte)(now.Year - 2000);
            Console.WriteLine("Setting time: {0}", csp2SetTime(timeset));
            csp2Restore();
        }
    }
}
